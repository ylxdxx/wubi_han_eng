// vim:fdm=syntax
// by tuberry
/* exported Fields */
'use strict';

var Fields = {
    CUSTOMFONT:    'custom-font',
    UPDATESDIR:    'updates-dir',
    CHECKUPDATES:  'check-updates',
    ENABLEDIALOG:  'enable-dialog',
    ENABLEUPDATES: 'enable-updates',
    MSTHEMECOLOR:  'ms-theme-color',
    ENABLEMSTHEME: 'enable-ms-theme',
    INPUTLIST:     'input-mode-list',
    MSTHEMESTYLE:  'default-variant',
    USECUSTOMFONT: 'use-custom-font',
    PAGEBUTTON:    'hide-page-button',
    UNKNOWNMODE:   'unkown-input-mode',
    AUTOSWITCH:    'enable-auto-switch',
    ENABLEORIEN:   'enable-orientation',
    ENABLECLIP:    'enable-clip-history',
    ORIENTATION:   'candidate-orientation',
    CLIPPAGESIZE:  'clip-history-page-size',
    RUNSHORTCUT:   'ibus-tweaker-run-dialog',
    CLIPHISTCUT:   'ibus-tweaker-clip-history',
};
